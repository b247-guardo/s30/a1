db.fruits.aggregate([
    { $count: "fruitsOnSale" }
  ]);

db.fruits.aggregate([
  { $match: {stock: { $gte: 20}}},
  { $count: "enoughStock" }
]);

db.fruits.aggregate([
  { $group: { _id: "$supplier_id", average_price: { $avg: "$price" } } }
]);

db.fruits.aggregate([
  { $group: { _id: "$supplier_id", max_price: { $max: "$price" } } }
]);

db.fruits.aggregate([
	{ $group: { _id: "$supplier_id", min_price: { $min: "$price" } } }
]);
